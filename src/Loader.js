import React from 'react';

const Loader = () => {
  return (
    <div className="todo__loader">
      <div className="lds-dual-ring"></div>
    </div>
  );
};

export default Loader;
